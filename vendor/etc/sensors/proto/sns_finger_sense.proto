// @file sns_finger_sense.proto
//
// Defines the API for Finger_Sense Sensors.
// All Hall Sensor drivers are required to comply with this API.
// Any new functionality for Hall Sensor can be defined in a
// device specific API file.
//
// Copyright (c) 2016-2018 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.

syntax = "proto2";
import "nanopb.proto";

// Attribute requirements:
// The Hall Sensor publishes:
// 1. SNS_STD_SENSOR_ATTRID_TYPE attribute value as "hall".
// 2. SNS_STD_SENSOR_ATTRID_RESOLUTIONS attribute is not applicable since this is an event
//    sensor.
// 3. SNS_STD_SENSOR_ATTRID_RANGES attribute values as 0 and 1.
// 4. See sns_std_sensor.proto for other attributes.

// Handling stream requests:
// 1. The Hall Sensor handles the SNS_STD_SENSOR_MSGID_SNS_STD_ON_CHANGE_CONFIG
//    message ID for all stream enable/update requests.
// 2. The Hall Sensor operates in on-change mode and report
//    samples only for magnet NEAR/FAR transitions.

// Message IDs for finger_sense Sensor
enum sns_finger_sense_msgid {
  option (nanopb_enumopt).long_names = false;

  // Uses message: sns_finger_sense_event
  // Purpose: An output data event from the hall sensor to it's client.
  SNS_FINGER_SENSE_MSGID_SNS_FINGER_SENSE_EVENT  = 819;
}

// Handling stream events:
// 1. The Hall Sensor publishes magnetic field proximity data stream
//    events using the sns_finger_sense_event message.
// 2. Each stream event publishes an accuracy field:
//    SNS_STD_SENSOR_SAMPLE_STATUS_UNRELIABLE to mark invalid samples when hardware is
//    yet to stabilize after the sensor is configured.
//    SNS_STD_SENSOR_SAMPLE_STATUS_ACCURACY_HIGH to mark samples when they are valid.
// 3. The Hall Sensor publishes a configuration event using the
//    sns_std_sensor_physical_config_event message.
//    It publishes this event each time there is change in hardware config of the sensor
//    and contains current physical sensor config of the sensor.

// Handling self-test requests:
// 1. The Hall Sensor implements SNS_PHYSICAL_SENSOR_TEST_TYPE_COM test
//    type using the physical sensor test API.
// 2. The Hall Sensor could implement other test types.

// Handling test events:
// 1. The Hall Sensor uses sns_physical_sensor_test_event message to publish
//    a test completion event.
// 2. The test_passed field in sns_physical_sensor_test_event is used to output the
//    pass/fail result of self-test execution.
// 3. The test_data field in sns_physical_sensor_test_event could be used to output any
//    driver-specific error data.
